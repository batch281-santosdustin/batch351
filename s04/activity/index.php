<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s04 Activity</title>
</head>
<body>

	<h1>Display Information</h1>

	<h2>Person Information</h2>
	<p>Name: <?= $person->getName(); ?> </p>
	<p>Age: <?= $person->getAge(); ?> </p>
	<p>Address: <?= $person->getAddress(); ?> </p>

	<h2>Student Information</h2>
	<p>Name: <?= $student->getName(); ?> </p>
	<p>Age: <?= $student->getAge(); ?> </p>
	<p>Student ID: <?= $student->getStudentId(); ?> </p>
	<p>Address: <?= $student->getAddress(); ?> </p>

	<h2>Employee Information</h2>
	<p>Name: <?= $employee->getName(); ?> </p>
	<p>Age: <?= $employee->getAge(); ?> </p>
	<p>Team: <?= $employee->getTeam(); ?> </p>
	<p>Role: <?= $employee->getRole(); ?> </p>
	<p>Address: <?= $employee->getAddress(); ?> </p>

</body>
</html>