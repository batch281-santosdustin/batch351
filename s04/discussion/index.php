<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Access Modifiers and Encapsulation</title>
</head>
<body>

	<h1>Access Modifiers</h1>

	<p>
		<em>
			Access Modifiers specify the scope of a class properties or methods and how they can be accessed.
		</em>
	</p>

	<h2>Building Object</h2>
	<p><?= $building->printName()?></p>
	<pre><?php var_dump($building); ?></pre>

	<h2>Condominium Object</h2>
	<p><?= $condominium->printName(); ?></p>
	<pre><?php var_dump($condominium); ?></pre>

	<h2>Building Variable</h2>
	<p><?= $building->getName(); ?></p>

	<h2>Condominium Variable</h2>
	<p><?= $condominium->getName(); ?></p>

	<h1>Encapsulation</h1>

	<h2>Condominium Object</h2>
	<p>The name of condominium is <?= $condominium->getName(); ?>.</p>
	<p>The floors of <?= $condominium->getName(); ?> are <?= $condominium->getFloor(); ?>.</p>
	<!-- <p>The address of <?= $condominium->getName(); ?> is <?= $condominium->getAddress(); ?>.</p> -->

	<?php $condominium->setName('Enzo Tower'); ?>
	

	<p>The name of condominium is <?= $condominium->getName(); ?>.</p>

	<h1>Abstraction</h1>
	<p>
		<em>
			Abstraction is used to create a blueprint for other classes (concrete class) to follow, without providing a complete implementation.
		</em>
	</p>

	<p><?= $dog->getName() . " says \"  . $dog->makeSound(); ?></p>
	<p><?= $cat->getName() . " says \"  . $cat->makeSound(); ?></p>

</body>
</html>