<?php

class Building {

	// public access modifier: fully open, properties and methods can be accessed from anywhere.
	// public $name;
	// public $floor;
	// public $address;

	// private access modifier: method and property can only be accessed within the class.
	// private $name;
	// private $floor;
	// private $address;

	// protected access modifier: property or method is only accessible within the class and its child class.
	protected $name;
	protected $floor;
	protected $address;

	public function __construct($name, $floor, $address){
		$this->name = $name;
		$this->floor = $floor;
		$this->address = $address;
	}

	// Encapsulation
	// THis indicates that data should not be directly accessible to users but through getter and setter functions.

	// getter(accessors) : used to retrieve the value of a property
	public function getName(){
		return $this->name;
	}

	public function getFloor(){
		return $this->floor;
	}

	public function getAddress(){
		return $this->address;
	}

	// setter(mutators) : used to modify the value of a property.
	public function setName($name){
		$this->name = $name;
	}

	private function setFloor($floor){
		$this->floor = $floor;
	}

	private function setAddress($address){
		$this->address = $address;
	}

	public function printName(){
		return "The name of the building is $this->name";
	}
}

$building = new Building ('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');


class Condominium extends Building{
	public function printName(){
		return "The name of condominium is $this->name";
	}
}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Ave, Makati City, Philippines');

// [SECTION] Abstraction

// Abstract Class
// Define abstract class
// It can't be instantiated but serves as a blueprint
abstract class Animal{
	protected $name;

	public function __construct($name){
		$this->name = $name;
	}

	// Abstract method
	// this is an abstract method that must be implemented by concrete subclasses
	abstract public function makeSound();

	// Concrete method
	public function getName(){
		return $this->name;
	}

	private function setName(){
		$this->name = $name;
	}
}

// $animal = new Animal ("Goldie");

// Create a concrete subclasses
class Dog extends Animal{
	public function makeSound(){
		return "Woof!";
	}
}

class Cat extends Animal{
	public function makeSound(){
		return "Meow!";
	}
}

// Create instances of the subclasses
$dog = new Dog("Ally");
$cat = new Cat("Mimay");