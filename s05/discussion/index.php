<?php

	// [SECTION] Super Global Variables
	// Superglobal Variables in PHP are predefined "global variables", which means that they can be used whenever needed.
	// Superglobal variables are written with "$" followed by an "_" and uppercase letter

	// $_GET and $_POST are examples of Super global variables in PHP
	
	// has data input in the form of GET requests.
	// echo '$_GET: <br/>';
	// var_dump($_GET);

	// echo '<br/> <br/>';

	// stores data input in the form of POST requests
	// echo '$_POST: <br/>';
	// var_dump($_POST);

	$tasks = ["Get Git", "Bake HTML", "Eat CSS", "Learn PHP"];

	// isset is a built-in function of PHP, which is used to determine if the variable is set or not
	if(isset($_GET['index'])){
		$indexGet = $_GET['index'];

		echo "The retrieve task from GET is $tasks[$indexGet]";
	}

	if(isset($_POST['index'])){
		$indexPost = $_POST['index'];

		echo "The retrieve task from GET is $tasks[$indexPost]";
	}
	// Store the value of "index" in a variable

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Client-Server Communication</title>
</head>
<body>
	<h1>Task index from GET</h1>

	<form method="GET">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">GET</button>
	</form>

	<h1>Task index from POST</h1>

	<form method="POST">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">POST</button>
	</form>
</body>
</html>