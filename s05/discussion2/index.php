<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Client-Server Communication(To Do App)</title>
	</head>
	<body>
		<!-- Initializes a new session or resume an existing one -->
		<?php session_start(); ?>

		<h3>Add Task</h3>
		<form method="POST" action="./server.php">
			<!-- This will determine the action to be performed by the server -->
			<input type="hidden" name="action" value="add">
			Description: <input type="text" name="description" required>
			<button type="submit">Add</button>
		</form>

		<h3>Task List</h3>
		<!-- Check if 'tasks' session variable is already existing -->
		<?php if(isset($_SESSION['tasks'])): ?>
			<!-- Assigns each task to the variable and retrieve the index -->
			<?php foreach($_SESSION['tasks'] as $index => $task): ?>
				<div>
					<!-- Update Task Form -->
					<form method="POST" action="./server.php" style="display:inline-block;">
						<!-- Hidden Input -->
						<input type="hidden" name="action" value="update" />
						<!-- index number of the task to be updated -->
						<input type="hidden" name="id" value="<?= $index ?>" />

						<!-- Checkbox for Task Completion -->
						<input type="checkbox" name="isFinished" <?= ($task->isFinished) ? 'checked' : null; ?> />

						<!-- Task Description UPdate -->
						<input type="text" name="description" value="<?= $task->description; ?>" />

						<button type="submit">Update</button>
					</form>

					<!-- Remove task form -->
					<form method="POST" action="./server.php" style="display:inline-block;">
						<input type="hidden" name="action" value="remove" />
						<input type="hidden" name="id" value="<?= $index; ?>">

						<button type="submit">Delete</button>
					</form>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>

		<br><br>

		<form method="POST" action="./server.php">
			<input type="hidden" name="action" value="clear" />
			<button type="submit">Clear all tasks</button>
		</form>
	</body>
</html>