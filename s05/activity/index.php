<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login Page</title>
</head>
<body>
    <?php session_start(); ?>

    <?php if (isset($_SESSION['email'])): ?>
        <!-- User is logged in -->
        <h3>Hello, <?= $_SESSION['email']; ?>!</h3>
        <form method="POST" action="server.php">
            <input type="hidden" name="action" value="logout">
            <button type="submit">Logout</button>
        </form>
    <?php else: ?>
        <!-- User is not logged in or login failed -->
        <?php if (isset($_SESSION['login_error_message'])): ?>
            <p><?= $_SESSION['login_error_message']; ?></p>
            <?php unset($_SESSION['login_error_message']); ?>
        <?php endif; ?>

        <form method="POST" action="server.php">
            <input type="hidden" name="action" value="login">
            Username: <input type="text" name="username" required>
            Password: <input type="password" name="password" required>
            <button type="submit">Login</button>
        </form>
    <?php endif; ?>
</body>
</html>
