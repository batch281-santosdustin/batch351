<?php
session_start();

class User {
    public function login($username, $password) {
        // Correct credentials
        $correctUsername = "johnsmith@gmail.com";
        $correctPassword = "1234";

        if ($username === $correctUsername && $password === $correctPassword) {
            $_SESSION['email'] = $correctUsername;
        } else {
            $_SESSION['login_error_message'] = "Incorrect username or password";
        }
    }

    public function logout() {
        session_destroy(); // Destroy the entire session
    }
}

$user = new User();

if (isset($_POST['action'])) {
    if ($_POST['action'] === 'login') {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $user->login($username, $password);
    } elseif ($_POST['action'] === 'logout') {
        $user->logout();
    }
}

header('Location: index.php');
?>
