<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s02 Activity</title>
</head>
<body>
	<h1>Divisible of Five</h1>
	<p><?php printDivisibleOfFive(); ?></p>

	<h1>Array Manipulation</h1>
	<?php array_push($students, 'John Smith'); ?>
	<p><?php var_dump($students); ?></p>
	<p><?= count($students); ?></p>
	<?php array_push($students, 'Jane Smith'); ?>
	<p><?php var_dump($students); ?></p>
	<p><?= count($students); ?></p>
	<?php array_shift($students); ?>
	<p><?php var_dump($students); ?></p>
	<p><?= count($students); ?></p>

</body>
</html>