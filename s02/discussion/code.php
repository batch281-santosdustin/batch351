<?php

// [SECTION] Repetition Control Structures
// It is used to execute code multiple times.

// While loop
function whileLoop(){
	$count = 5;

	while($count !== 0){
		echo $count.'<br/>';
		$count--;
	}
}

// Do-While Loop
function doWhileLoop(){
	$count = 20;

	do{
		echo $count.'<br/>';
		$count--;
	} while ($count > 20);
}

// For Loop
function forLoop(){
	for($count = 0; $count <= 20; $count++){
		echo $count.'<br/>';
	}
}

// Continue and Break
function modifiedForLoop(){
	for($count = 0; $count <= 20; $count++){
		// continue
		if($count % 2 === 0){
			continue;
		}

		echo $count.'<br/>';

		// break
		if($count > 10){
			break;
		}
	}
}

// [SECTION] Array
	// In PHP, arrays can be declared using array() function or square bracket '[]'.

	$studentNumbers = array('2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'); //before PHP 5.4
	$studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']; // introduce on PHP 5.4

	// Array Types in PHP
	// There are different kind of arrays in PHP and each array value is accessed using "array index".

	// Simple Array (or Numeric Array)
	$grades = [98.5, 94.3, 89.2, 90.1];
	$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
	$tasks = [
		'drink html',
		'eat javascript',
		'inhale css',
		'bake sass'
	];

	// Associative Array (using string index)
	$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

	// Multidimensional Array

	// Two-Dimensional Simple Array
	$heroes = [
		['iron man', 'thor', 'hulk'],
		['wolverine', 'cyclops', 'jean grey'],
		['batman', 'superman', 'wonder woman']
	];

	// Two-Dimensional Associative Array
	$ironManPowers = [
		'regular' => ['repulsor blast', 'rocket punch'],
		'signature' => ['unibeam']
	];

	// Array Functions (or Methods)
	// PHP arrays are essential for storing, managing, and operationg on sets of variables.

	// Array Mutations
	
	// Sorting Arrays
	$sortedBrands = $computerBrands;
	$reverseSortedBrands = $computerBrands;

	// Sorting arrays (changes the array itself)
	sort($sortedBrands); //A-Z
	rsort($reverseSortedBrands); //Z-A

	// Other Array Functions

	// in_array()
	function searchBrand($brands, $brand){
		return (in_array($brand, $brands)) ? "$brand is in the array." : "$brand is not in the array.";
	}

	// array_reverse()
	array_reverse($computerBrands);
	$reverseComputerBrands = array_reverse($computerBrands);