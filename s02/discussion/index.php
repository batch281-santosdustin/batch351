<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Repetition Control Structures and Array Manipulation</title>
</head>
<body>

	<h1>Repetition Control Structures</h1>

	<h2>While Loop</h2>
	<?php whileLoop(); ?>

	<h2>Do-While Loop</h2>
	<?php doWhileLoop(); ?>	

	<h2>For Loop</h2>
	<?php forLoop(); ?>

	<h2>Continue and Break</h2>
	<?php modifiedForLoop(); ?>

	<h2>Array</h2>
	<h2>Student array: <?php print_r($studentNumbers); ?></h2>

	<h2>Types of Arrays</h2>
	<h3>Simple Array</h3>

	<!-- 
		Syntax:
			foreach(iterable_expression as $value){
				//code block to be executed
			}
			- foreach loop iterates over each element in the "iterable_expression" and assigns the current element's value to the variable "$value".
	-->

	<ul>
		<?php foreach($computerBrands as $brand){ ?>
			<!-- This is a shorthand method for php echo -->
			<li><?= $brand; ?></li>
		<?php } ?>
	</ul>

	<h3>Associative Array</h3>
	<!-- 
		Syntax:
			foreach (iterable_expression as $key => $value){
				// code block to execute for each key and value
			}

			- foreach loop doesn't only assign the current element's value to variable $value but also assigns the current element's key (index) to the variable $key
			- in this syntax, you can access both the key ($key) and the value ($value) of the current element and perform actions based on both. 
	 -->

	 <ol>
	 	<?php foreach($gradePeriods as $period => $grade){ ?>
	 		<li>
	 			Grade in <?= $period; ?> is <?= $grade; ?>
	 		</li>
	 	<?php } ?>
	 </ol>

	 <h3>Multidimensional Simple Array</h3>
	 <ul>
	 	<?php 
	 		foreach($heroes as $team){
	 			foreach($team as $member){
	 	?>
	 		<li><?= $member; ?></li>
	 	<?php 
	 			}
	 		}
	 	 ?>
	 </ul>

	 <h3>Multidimensional Associative Array</h3>

	 <ol>
	 	<?php 
	 		foreach($ironManPowers as $label => $powerGroup){
	 			foreach($powerGroup as $power){
	 	?>
	 		<li>
	 			<?= "$label: $power"; ?>
	 		</li>
	 	<?php 
	 			}
	 		}
	 	?>
	 </ol>

	 <h2>Array Functions</h2>

	 <h3>Original Array</h3>

	 <pre><?php print_r($computerBrands); ?></pre>

	 <h3>Append</h3>

	 <h4>array_push()</h4>
	 <h5>Add one or more element on the end of an array</h5>
	 <p>Returns the new length: <?= array_push($computerBrands, 'Apple'); ?></p>
	 <pre><?php print_r($computerBrands); ?></pre>

	 <h4>array_unshift()</h4>
	 <h5>Add one or more element on the start of an array</h5>
	 <p>Returns the new length: <?= array_unshift($computerBrands, 'Dell') ?></p>
	 <pre><?php print_r($computerBrands); ?></pre>

	 <h3>Remove</h3>

	 <h4>array_pop()</h4>
	 <h5>Remove the element off in the end of an array</h5>
	 <p>Returns the removed item: <?= array_pop($computerBrands) ?></p>
	 <pre><?php print_r($computerBrands); ?></pre>

	 <h4>array_shift()</h4>
	 <h5>Remove the element off in the start of an array.</h5>
	 <p>Returns the removed item: <?= array_shift($computerBrands) ?></p>
	 <pre><?php print_r($computerBrands); ?></pre>

	 <h3>Sorting Array in Ascending Order</h3>
	 <pre><?php print_r($sortedBrands); ?></pre>

	 <h3>Sorting Array in Descending Order</h3>
	 <pre><?php print_r($reverseSortedBrands); ?></pre>

	 <h3>Other Array Functions</h3>
	 <h4>Count</h4>
	 <p>Total length of $computerBrands: <?= count($computerBrands) ?>;</p>

	 <h4>In Array</h4>
	 <p><?= searchBrand($computerBrands, 'HP'); ?></p>
	 <p><?= searchBrand($computerBrands, 'Acer'); ?></p>

	 <h4>Reverse Order (not sorting)</h4>
	 <pre><?php print_r($computerBrands); ?></pre>
	 <pre><?php print_r($reverseComputerBrands); ?></pre>

</body>
</html>