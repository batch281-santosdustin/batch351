<?php 
	function getFullAddress($country, $city, $province, $specificAddress) {
		return "$specificAddress, $city, $province, $country";
	}

	function getLetterGrade($grade) {
	    if ($grade >= 98 && $grade <= 100) {
	        return "$grade is A+";
	    } else if ($grade >= 95 && $grade <= 97) {
	        return "$grade is A";
	    } else if ($grade >= 92 && $grade <= 94) {
	        return "$grade is A-";
	    } else if ($grade >= 89 && $grade <= 91) {
	        return "$grade is B+";
	    } else if ($grade >= 86 && $grade <= 88) {
	        return "$grade is B";
	    } else if ($grade >= 83 && $grade <= 85) {
	        return "$grade is B-";
	    } else if ($grade >= 80 && $grade <= 82) {
	        return "$grade is C+";
	    } else if ($grade >= 77 && $grade <= 79) {
	        return "$grade is C";
	    } else if ($grade >= 75 && $grade <= 76) {
	        return "$grade is C-";
	    } else if ($grade <= 74){
	        return "$grade is F";
	    } else {
	    	return "Invalid";
	    }
	}

?>