<!--
	1. Create code.php and index.php files inside the activity folder.
	2. In code.php, create a function named getFullAddress() that will take four arguments:
	    - Country
	    - City
	    - Province
	    - Specific Address (such as block, lot, or building name and room number).
	3. Have this function return the concatenated arguments to result in a coherent address.
	4. Create another function named getLetterGrade() that uses conditional statements to output a letter representation of a given numerical grade:
	    - A+ (98 to 100)
	    - A (95 to 97)
	    - A- (92 to 94)
	    - B+ (89 to 91)
	    - B (86 to 88)
	    - B- (83 to 85)
	    - C+ (80 to 82)
	    - C (77 to 79)
	    - C- (75 to 76)
	    - F (75 below)
	5. Include the code.php in the index.html and invoke the created methods.
-->

<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s01 Activity</title>
</head>
<body>
	<h1>Full Address</h1>
	<p><?php echo getFullAddress("Philippines", "Bulacan", "Marilao", "0001 Antonio Compound, Metrogate Complex, Lias") ?></p>
	<p><?php echo getFullAddress("Philippines", "Bulacan", "Obando", "21B Lawa") ?></p>

	<h1>Letter Based Grading</h1>
	<p><?php echo getLetterGrade(87); ?></p>
	<p><?php echo getLetterGrade(94); ?></p>
	<p><?php echo getLetterGrade(74); ?></p>
</body>
</html>