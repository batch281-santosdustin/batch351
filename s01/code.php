<?php
	/*
		To start the PHP Development Server in the terminal and serve files inside the directory:

		php -S localhost:8000
	*/

	/*
	- PHP code can be included to another file by using the require_once keyword.
		- "index.php" for embedding php in HTML to be serve and shown in our browser.
		- "code.php" for defining php statements such as variables, statements, and functions.
	*/


	// [SECTION] Comments
		// Comments are meant to describe the written code.

		/*
			There are two types of comments:
			- Single line comment denoted by two forward slashes.
			- Multi-line comment denoted by a forward slash and asterisk 

		*/

	// [SECTION] Variables
		// Variables are defined using the dollar($) sign notation before the variable name.

	$name = 'John Smith';
	$email = "johnsmith@gmail";

	// [SECTION] Constants
		// Constants are defined using the define() function.
			// Naming convetions for "constant" should be in ALL CAPS.
			// It doesn't use the "$" notation before the variable name.

	define("PI", 3.1416);

	// Variable vs Constant
		// Constants do not follow any variable scoping rules, and they can be defined and accessed anywhere.
		// Variables can be declared anywhere in the program, but they follow variable scoping rules.

	// [SECTION] Data Types

	// Strings
	$state = "New York";
	$country = 'United States of America';
	$address = $state.', '.$country; //concatetnation via . notation
	// $address = "$state, $country"; concatention via double quote.

	// Integers
	$age = 31;
	$headcount = 26;

	// Floats
	$grade = 98.2;
	$distanceInKilometers = 1342.12;

	// Boolean
	$hasTravelledAbroad = false;
	$haveSymptoms = true;

	// Arrays
		// array() is used to create an array in PHP.
			 // '[]' will also work in creating array.
	$grades = array(98.7, 92.1, 90.2, 94.6);

	// Null
	$spouse = null;
	$middleName = null;

	// Objects
	// Objects in PHP are used to model more complex data structures and encapsulate data and behavior.
	// fat arrow (=>) is used to assign key value pairs.

	$gradesObj = (object)[
		'firstGrading' => 98.7,
		'secondGrading' => 92.1,
		'thirdGrading' => 90.2,
		'fourthGrading' => 94.6
	];

	$personObj = (object)[
		'fullName' => 'John Smith',
		'isMarried' => false,
		'age' => 35,
		'address' => (object)[
			'state' => 'New York',
			'country' => 'United States of America'
		]
	];

	// [SECTION] Operators

	// Assignment Operator

	$x = 100;
	$y = 25;

	$isLegalAge = true;
	$isRegistered = false;

	// [SECTION] Functions
	// function getFullName($firstName, $middleInitial, $lastName){
	// 	return "$lastName, $firstName $middleInitial";
	// }

	// arrow Function for php
	$fullName = fn ($firstName, $middleInitial, $lastName) => "$lastName, $firstName $middleInitial";

	// [SECTION] Selection Control Structure

	// If-Elseif-else Statement
	function determineTyphoonIntensity($windSpeed){
		if($windSpeed < 30){
			return 'Not a typhoon yet.';
		}
		else if($windSpeed <= 61){
			return 'Tropical depression detected.';
		}
		else if($windSpeed >= 62 && $windSpeed <= 88){
			return 'Tropical storm detected.';
		}
		else if($windSpeed >= 89 && $windSpeed <= 117){
			return 'Severe tropical storm detected.';
		}
		else{
			return 'Typhoon detected.';
		}
	}

	// Ternary Operator
	function isUnderAge($age){
		return ($age < 18) ? true : false;
	}

	// Switch Case Statement
	function determineComputerUsers($computerNumber){
		switch ($computerNumber) {
			case 1:
				return 'Linus Torvalds';
				break;
			case 2:
				return 'Steve Jobs';
				break;
			case 3:
				return 'Sid Meier';
				break;
			case 4:
				return 'Onel De Guzman';
				break;
			case 5:
				return 'Christian Salvador';
				break;
			default:
				return "$computerNumber is out of bounds.";
				break;
		}
	}

	// Try-Catch-Finally Statement
	function greeting($str) {
	    try {
	        // Attempt to execute a code.
	        if(gettype($str) === "string"){
	            echo $str;
	        }else{
	            //instantiate a new Exception object from PHP's pre-defined Exception class using its constructor
	            //the Exception object created here will have a message "Oops!"
	            //classes and objects will be discussed in further details in a future session, this is just to demonstrate triggering of a catch block
	            throw new Exception("Oops!");
	        }
	    }
	    catch (Exception $e) {
	        // Catch errors within 'try', in this case the error is not a "string" data type.
	        // $e is an object we a getter function "getMessage()"
	        echo $e->getMessage();
	    }
	    finally {
	        // Continue execution of code regardless of success and failure of code execution in 'try' block.
	        echo " I did it again!";
	    }
	}
	
