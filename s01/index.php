<!-- Used to include another php file -->
<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP Basics and Selection Control Structure</title>
</head>
<body>
		<!-- <h1>Hello world!</h1> -->

		<h1>Echoing Values</h1>
		<!-- Echo() is used to output data to the screen. -->

		<!-- It can easily read variables -->
		<p><?php echo "Good day $name! Your given email is $email."; ?></p>
		<p><?php echo 'Good day $name! Your given email is $email.'; ?></p>

		<!-- Single quote can be used but concatenation is needed -->
		<!-- dot(.) is used for concatenation -->
		<p><?php echo 'Good day' . $name . '!' . 'Your given email is' . $email . '.'; ?></p>

		<!-- Constant variable uses the dot(.) to display its value. -->
		<p><?php echo "The value of PI is " . PI; ?></p>

		<h1>Data Types</h1>

		<!-- Boolean and Null variable will not be visible using echo -->
		<p>hasTravelledAbroad: <?php echo "$hasTravelledAbroad"; ?></p>
		<p>spouse: <?php echo "$spouse"; ?></p>

		<!-- To see their types instead, we can use gettype() or vardump() function. -->

		<!-- gettype() function -->
		<p>hasTravelledAbroad: <?php echo gettype($hasTravelledAbroad); ?></p>
		<p>spouse: <?php echo gettype($spouse); ?></p>

		<!-- var_dump() function -->
		<p>hasTravelledAbroad: <?php var_dump($hasTravelledAbroad); ?></p>
		<p>spouse: <?php var_dump($spouse); ?></p>

		<!-- 
			Note:
				- gettype() : returns the data type of the variable value passed as an argument
				- var_dump() : displays structured information about variables/ expressions including its type and value
		-->

		<!-- To output the value of an object property, the arrow notation can be used. -->
		<p><?php echo $gradesObj -> firstGrading; ?></p>
		<p><?php echo $personObj -> address -> state; ?></p>

		<!-- To output an array element, we can use the usual suqare brackets -->
		<p><?php echo $grades[3]; ?></p>
		<p><?php echo $grades[2]; ?></p>

		<!-- If we want to display the whole array and object we can use var_dump() -->
		<p><?php var_dump($gradesObj); ?></p>
		<p><?php var_dump($grades); ?></p>

		<!-- print_r() function can also be used to print the whole array and object -->
		<p><?php print_r($personObj); ?></p>
		<p><?php print_r($grades); ?></p>
		
		<!-- Note: print_r() displays information about a variable in a way that's readable by humans. -->

		<h1>Operators</h1>

		<p>X: <?php echo $x; ?></p>
		<p>Y: <?php echo $y; ?></p>

		<p>Is Legal Age: <?php echo var_dump($isLegalAge); ?></p>
		<p>Is Registered: <?php echo var_dump($isRegistered); ?></p>

		<h2>Arithmetic Operators</h2>
		<p>Sum: <?php echo $x + $y; ?></p>
		<p>Difference: <?php echo $x - $y; ?></p>
		<p>Product: <?php echo $x * $y; ?></p>
		<p>Quotient: <?php echo $x / $y; ?></p>
		<p>Modulo: <?php echo $x % $y; ?></p>

		<!-- += also exists in php -->

		<h3>Equality Operators</h3>

		<p>Loose Equality: <?php var_dump($x == '100'); ?></p>
		<p>Stirct Equality: <?php var_dump($x === '100'); ?></p>
		<p>Loose Inequality: <?php var_dump($x != '100'); ?></p>
		<p>Stict Inequality: <?php var_dump($x !== '100'); ?></p>

		<!-- Strict equality/ inequality is preferred so that we can check for both of the value and data type two given values. -->

		<h3>Greater/ lesser operators</h3>
		<p>Is Lesser: <?php var_dump($x < $y); ?></p>
		<p>Is Greater: <?php var_dump($x > $y); ?></p>

		<p>Is Lesser or Equal: <?php var_dump($x <= $y); ?></p>
		<p>Is Greater or Equal: <?php var_dump($x >= $y); ?></p>

		<h2>Logical Operators</h2>

		<!-- Logical operators are used to verify whether an expression or group of expressions are either true or false -->

		<p>Are All Requirements Met: <?php var_dump($isLegalAge && $isRegistered); ?></p>
		<p>Are Some Requirements Met: <?php var_dump($isLegalAge || $isRegistered); ?></p>
		<p>Are Some Requirements Not Met: <?php var_dump(!$isLegalAge && !$isRegistered ); ?></p>

		<h1>Functions</h1>
		<p>Full Name: <?php echo $fullName('John', 'D', 'Smith'); ?></p>

		<h1>Selection Control Structures</h1>

		<h2>If-Elseif-Else Statement</h2>

		<p><?php echo determineTyphoonIntensity(120); ?></p>

		<h2>Ternary Operator (isUnderage)</h2>
		<p>78: <?php var_dump(isUnderAge(78)); ?></p>
		<p>16: <?php var_dump(isUnderAge(16)); ?></p>

		<h2>Switch Case</h2>
		<p><?php echo determineComputerUsers(5); ?></p>
		<p><?php echo determineComputerUsers(10); ?></p>

		<h2>Try-Catch-Finally</h2>
		<p><?php greeting("Hello"); ?></p>
		<p><?php greeting(25); ?></p>
</body>
</html>