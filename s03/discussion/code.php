<?php

// [SECTION] Objects
// Objects is a compound data type (along w/ array)
// Object is an instance of either a 'built-in' or 'user-defined' class

// [SECTION] Objects as variables
// The code below directly creates an object using the object literal notation "(object)[]" which uses the "stdClass".
// This is also an object instantiated on a 'built-in' class
$buildingObj = (object)[
	'name' => 'Caswynn Building',
	'floors' => 8,
	'address' => (object)[
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];

$buildingObj2 = (object)[
	'name' => 'Caswynn Building',
	'floors' => 8,
	'address' => (object)[
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];

// [SECTION] Classes
// A "Class" is a programmer-defined data type which includes local functions (methods) as well as local variable/ data (properties)
// "Class" is a "blueprint" on creating objects with same structure and behavior

// [SECTION] Objects from Classes
//

class Building {
	// properties/ attributes
	// Characteristics of the object
	public $name;
	public $floor;
	public $address;

	// A constructor is used during the object creation/ instantiation.
	// This will initialize the object's properties with the values provided when object is instantiated.

	public function __construct($name, $floor, $address){
		$this->name = $name;
		$this->floor = $floor;
		$this->address = $address;
	}

	// Methods
	// an action that an object can perform
	public function printName(){
		return "The name of the building is $this->name";
	}
}

// INstantiating the Building Class (Object Creation)
$building = new Building ('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

// Inheritance
	// The derived(child) classes are allowed to inherit all the properties and methods from a specified base(parent) class
	// "extends" keyword is used to derive a class from another class
class Condominium extends Building{
	// name, floor, and address are inherited in this class

	// Polymorphism
		// Methods inherited by a derived class can be overriden to have a behavior different from the method of the base class

	// This overrides the behavior of the printName from the Building Class.
	public function printName(){
		return "The name of condominium is $this->name";
	}
}

// Instantiating the Condominium class
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Ave, Makati City, Philippines');