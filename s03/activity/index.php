<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s03 Activity</title>
</head>
<body>

	<h1>Person</h1>
	<p><?= $person->printName(); ?></p>

	<h1>Developer</h1>
	<p><?= $developer->printName(); ?></p>

	<h1>Engineer</h1>
	<p><?= $engineer->printName(); ?></p>

</body>
</html>