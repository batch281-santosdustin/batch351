<!-- 
	1. In the code.php, create a Person class with three properties: firstName, middleName and lastName.
	2. Create two child classes for Person: Developer and Engineer.
	3. Create a printName() method in each of the classes and must have the following output:
	    - Person: "Your full name is Senku Ishigami."
	    - Developer: "Your name is John Finch Smith and you are a developer."
	    - Engineer: "You are an engineer named Harold Myers Reese."
	4. Instantiate all the created classes.
	5. Invoke all the printName() method in the index.php.
-->
<?php

class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}
}

class Developer extends Person {
	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and your are a developer.";
	}
}

class Engineer extends Person {
	public function printName(){
		return "Your are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
}

$person = new Person('Senku', '', 'Ishigami');
$developer = new Developer('John', 'Finch', 'Smith');
$engineer = new Engineer('Harold', 'Myers', 'Reese');